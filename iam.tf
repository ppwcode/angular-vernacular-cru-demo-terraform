data "azurerm_subscription" "ng-vernacular-cru-sub" {}

resource "azurerm_role_assignment" "contributor-role" {
  for_each             = toset(local.users)
  // scope                = data.azurerm_subscription.ng-vernacular-cru-sub.id
  scope                = azurerm_resource_group.ng-vernacular-cru.id // Access to resource group is enough
  role_definition_name = "Contributor"
  principal_id         = each.value
}
