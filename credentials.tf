provider "azurerm" {
  features {}

  subscription_id = "ee80e5c6-b8c9-4c96-bdf9-da3113fcfe0c"
}

provider "azuread" {
  tenant_id = "6bf21748-a694-4bd1-b1c4-6563b75bde06"
}

terraform {
  required_providers {
    azuread = {
      source = "hashicorp/azuread"
      version = "=2.10.0"
    }
    azurerm = {
      source = "hashicorp/azurerm"
      version = "=2.86.0"
    }
  }
}
