terraform {
  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "tfstatengvernacularcru"
    container_name       = "tfstate-angular-vernacular-cru-demo"
    key                  = "angular-vernacular-cru-terraform.tfstate"
    subscription_id      = "ee80e5c6-b8c9-4c96-bdf9-da3113fcfe0c"
  }
}

locals {
  location = "West Europe"
}
