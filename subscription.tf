// Create alias for existing Subscription
// see https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subscription
resource "azurerm_subscription" "ng-vernacular-cru-sub" {
  alias = "ng-vernacular-cru-sub"
  subscription_name = "Visual Studio Enterprise Subscription – MPN"
  subscription_id   = "ee80e5c6-b8c9-4c96-bdf9-da3113fcfe0c"
}
