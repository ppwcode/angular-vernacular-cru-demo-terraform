data "azuread_user" "bdemeulenaere" {
  user_principal_name = "bart_demeulenaere@peoplewarenv.onmicrosoft.com"
}

data "azuread_user" "gstaes" {
  user_principal_name = "glen_staes@peoplewarenv.onmicrosoft.com"
}

data "azuread_user" "hvangestel" {
  user_principal_name = "hans_van_gestel@peoplewarenv.onmicrosoft.com"
}

data "azuread_user" "jdockx" {
  user_principal_name = "jan_dockx@peoplewarenv.onmicrosoft.com"
}

data "azuread_user" "jtroosters" {
  user_principal_name = "johan_troosters@peoplewarenv.onmicrosoft.com"
}

data "azuread_user" "kdewinter" {
  user_principal_name = "kristof_de_winter@peoplewarenv.onmicrosoft.com"
}

data "azuread_user" "msomers" {
  user_principal_name = "maarten_somers@peoplewarenv.onmicrosoft.com"
}

data "azuread_user" "mvandepoel" {
  user_principal_name = "matthias_van_de_poel@peoplewarenv.onmicrosoft.com"
}

data "azuread_user" "rvandeginste" {
  user_principal_name = "ruben_vandeginste@peoplewarenv.onmicrosoft.com"
}

locals {
  users = [
    data.azuread_user.bdemeulenaere.object_id,
    data.azuread_user.gstaes.object_id,
    data.azuread_user.hvangestel.object_id,
    data.azuread_user.jdockx.object_id,
    data.azuread_user.jtroosters.object_id,
    data.azuread_user.kdewinter.object_id,
    data.azuread_user.msomers.object_id,
    data.azuread_user.mvandepoel.object_id,
    data.azuread_user.rvandeginste.object_id
  ]
}
